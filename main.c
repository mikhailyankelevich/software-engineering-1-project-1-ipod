//
//  The program is the first assingment in COMP 10050. It is a song shuffler,
//  at first it asks for artist and song name untill enter is entered and then prints out all songs and artists in alphabetical order
//  and shuffled songs which will occure only 2wice and no less than 5 songs willbe appart from the repeted ones.
//
//  Created by Mikhail Yankelevich.
//  Student number 16205326
//


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

char artists[4][255];//arrays for alphabet sorting, the ones user enter
char songs[4][3][255];

int choice_array[24];/*array which will save number of the songs, and will be shuffled,
                        so instead of the songs shuffling this program will shuffle their
                        numbers and just print them out from the arraays storring them*/
int n;//counter for choice_array( will depen on how many songs are going to be entered

//initializing all functions
int NAME_READ(void);
int randomizer(void);
int array_creator(void);
int check(int a, int j);
int name_alphabet_sort(void);
int output(void);
int print_out_sorted_array(void);



int main(void)
{
    
    NAME_READ();//getting the names of the songs and groups
    name_alphabet_sort();//sorting out names uin alphabetical order
    randomizer();//randomizing an array of song numbers (shuffling it)
    output();//giving user output of shuffled songs
    return 0;
}




int NAME_READ(void)/*This function is getting names of the artists and songs and putting them into 2 arrays of strings.
                    One of these arrays is 1 dimensional (the one for artists) and another 2 dimensional ( for songs, there is each
                    line shows artists and for each artist, there are 3 columns with his/her song).
                    it reads the artists name and then checks if it is not'\0'. If it is, then it stops. if not, it askes for songs
                    of this artist, again stops if the song is '\0'.
                    In order to change  \n at the end of the string to '\0', so it is easier to work with, after entering any string
                    it changes the last symbol to '\0'.*/
{
    for (int art=0; art<4; art++)
    {
        
        printf ("enter the %d artist\n", art+1);//asking to enter an atist name
        fgets(artists[art], 255, stdin);// entering an artist name
        
        long int i=strlen(artists[art])-1;// changing the last symbot of the string to /0 by finding the last sybol in the string
        if(artists[art][i] == '\n')
            artists[art][i] = '\0';
        
        
        if (artists[art][0]=='\0')// if element number 0  is '\0', then stopping the atrists loop
            break;
        
        
        
        for ( int song =0; song<3; song++)
        {
            //char song_name[255];
            printf(" enter the %d song of %d artist\n", song+1, art+1);//asking to enter a songs name
            fgets(songs[art][song], 255, stdin);// entering an songs name
            
            
            long int j=strlen(songs[art][song])-1;// changing the last symbot of the string to /0 by finding the last sybol in the string
            if(songs[art][song][j] == '\n')
                songs[art][song][j] = '\0';
            
            
            if (songs[art][song][0]=='\0')// if element number 0  is '\0', then stopping the songs loop
                break;
            
        }
        
    }
    return 0;
}





int name_alphabet_sort(void)/*this function is sorting out songs and artist in alphabetical order using Quicksort.
                             At first, this program sorts out songs for each artist in turn and then does artists.
                             this function does not care about '\0' names, so it always put
                             them at the beginning of the sorting array( or part of the array*/

{
    int minindex;
    

    for(int art=0; art<4; art++)//sorting songs for each artist in turn from 0 to 4
        for (int i = 0; i<2; i++)//for songs  from 0 to 1 set miniindex to i
        {
            minindex=i;
            for (int j= i+1; j<3; j++) //for songs from i+1 till the end of the array if song with minyindex is smaller then song with j set miniindex to j
            {
                if (strcmp(songs[art][minindex], songs[art][j])>0)
                {
                    minindex=j;
                    
                }
            }
            char a[255];
            char a2[255];
            strcpy(a,songs[art][minindex]);//swap song with i and minindex
            strcpy(a2,songs[art][i]);
            strcpy(songs[art][i],a);
            strcpy(songs[art][minindex],a2);
            
        }
    
    
    
    
    for (int i = 0; i<3; i++)//sorting out artists names by alphabetical order in quick sort ( exactly the same way as above
    {
        minindex=i;
        for (int j= i+1; j<4; j++)
        {
            if (strcmp(artists[minindex], artists[j])>0)
            {
                minindex=j;
                
            }
        }
        char a[255];
        char a2[255];
        strcpy(a,artists[minindex]);//swap artist with i and minindex
        strcpy(a2,artists[i]);
        strcpy(artists[i],a);
        strcpy(artists[minindex],a2);
        
        for (int t=0; t<3; t++) // swap all song of swaping artists, so they won't mix up after sorting and stay in alphabetical order
        {
            strcpy(a,songs[minindex][t]);
            strcpy(a2,songs[i][t]);
            strcpy(songs[i][t],a);
            strcpy(songs[minindex][t],a2);
        }
    }
    
    
    
   print_out_sorted_array(); //print out already sorted array
    return 0;
}





int print_out_sorted_array(void) /*This function prints out sorted array ignorring all '\0'
                                  Also while printing every song out ( all non '\0' ones) this
                                  function sves the 'numbers' of the songs in global
                                  'choice_array' array (artists number*3 + sogs number in this artists list).*/
{
    for (int art=0; art<4; art++)//for all 4 artists
    {
        if (artists[art][0]!='\0')//checking if artist is not an \0
        {
            printf("%s\n", artists[art]);//printing out the sorted artists name
        for (int song=0; song<3;song++)//for each artist check all its 3 songs
            {
                if (songs[art][song][0]!='\0')//checking if song is not \0
                {
                    printf("\t-\t%s\n",  songs[art][song]);// printing out the sorted song
                    choice_array[n]=(art*3+song);//saving the number of the song in thre choice_array array
                    n++;//moving number of elements for choice_array forward by 1
                }
            }
        }
    }
    return 0;
                     
}






int array_creator(void)/*this function coppies the first n elements(the numbers of the songs
                        which are not \0) in to array space from n to 2n( so every song will
                        be repieted twice in this arraay and in finall shufling) */
{
    for (int i=n; i<n*2; i++) //from n position in the array up to the 2*n copy choice_array element which was n elements before ( n is coping 0 and so on).
    {
            choice_array[i]=choice_array[i-n];
       
    }
    return 0;
}







int randomizer(void)/* This function is randomizing an array of song numbers ( choise_array),
                     it uses Fisher Yates algorithm, however due to the fact, that there are only 24 songs max
                     the shuffling is repeated n*2 times just to make the play list as random as it can be*/
{

    srand(time(NULL));// setting up a randomizer using time in the processor( trully random)
    
    array_creator();//calling out the function which sets up a choice_array for sorting and fills it
    
    int j;
    for( int rep=0; rep <n*2; rep++)//repeating the sorting for n*2 times, as describes in the description of the function
        for (int i=1; i<n*2; i++ )/*for each number in the array from 1 to n*2 pickint a random number lower then courent
                                   and if elements which are standing under this number satisfy all rules of the assighment swap them*/
    {
        
        j=rand()%(i+1);//picking a random number j less then i
        if (j!=i&&check(choice_array[i], j)==1 && check(choice_array[j], i)==1)//checking if all rulles will still apply after the change
        {
            int swap= choice_array[i];//swaping the elements of the array
            choice_array[i]=choice_array[j];
            choice_array[j]=swap;
       
        }
    }
    
    return 0;
}






int check(int a, int j)/*This function checkes if an element in choice_array suits rulles set in the assingment
                        these are : no repetition of the song more then twice and at least 5 songs in between of the repetition.
                        However, the second rule does not apply i there are less then 6 songs, as it would be impossible to shuffle at all.
                        The first rulle should not be checked, as choice_array will allways have just 2 same songs numbers in it, no more, no less.
                        this function will return 1 if everything is fine and 0  if there are any problem with the playsment of the number.
                        */
{
    if ( n>6)//checking if there are more then 6 songs
    for (int i=1; i<6; i++)//checking if the last 5 songs were similar
    {
        if (choice_array[j+i]==a )
            return 0;// if there is less then 5 songs appart from any side then returning 0
        if (choice_array[j-i]==a )
            return 0;
    }
            return 1;//if everythg is fine return 1
}






int output(void) /*outputting the result of the shuffle by calculating the number of artist and number of the song in his/her library ( all storred in global arrays artists and songs */
{
    int song;
    int art;
    printf("\n");
    
    for (int i =0; i<n*2; i++)//for all array calculating song number and artist number and finding right name of an artist and the song in global arrays artists and songs
    {
        art= (choice_array[i])/3;//calculating an artists number
        song= (choice_array[i]%3);//calculating a songs number
        printf("%s - %s\n ", artists[art], songs[art][song]);//printing out the final results
    }
    
    return 0;
}

